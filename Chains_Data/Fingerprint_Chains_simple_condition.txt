Source [1611, 1825, 2153] -> Targets [1800]
	1611: r.ia iao- t.nt nuta (C) 1478 (Q)
	1825: s.o: a.ce uso- tesc (C) 1468 (A)
	2153: s.o: a.ce uso- tesc (C) 1498 (A)
	1949: o-r- eri* t.nt apon (C) 1503 (A)
	1801: o-r- ncnc uso- hisi (3) 1626 (R)
	2189: o-r- ncnc uso- hisi (3) 1626 (R)
	1799: o-r- ncnc uso- hisi (3) 1639 (R)
	1800: uio- ncnc uso- hisi (3) 1647 (R)

Source [1650, 1781, 1925, 2076, 2211] -> Targets [2254 1930 1790 1764 1908]
	1925: aqta m.um ece- leli (C) 1490 (R)
	1926: aqta m.n- ece- asua (C) 1491 (R)
	1930: aqta m.o- eris tain (3) 1501 (A)
	2211: niea irs- a.n- nute (C) 1507 (T)
	1650: exre m.ru upst ta&i (3) 1513 (R)
	1942: exre m.ru upse ta&i (3) 1519 (R)
	1883: ++++ m.ru s.am nute (C) 1537 (R)
	1884: o-et m.a- s.am nute (C) 1537 (R)
	1682: o-et m.um s.am nute (C) 1541 (R)
	1687: o-et m.de s.um nute (C) 1545 (R)
	2076: i-i- nti- lae) plau (7) 1550 (A)
	1781: t.s? a-em s.i- quui (3) 1552 (A)
	2104: t.s* i-is s.b- inqu (3) 1556 (A)
	2105: t.s? **eu s.am inqu (3) 1560 (A)
	2148: t.s? o*eu s.am inqu (3) 1560 (R)
	1784: o-,& ,&t. s.am inqu (3) 1561 (A)
	1862: i-i- o-e- s.i- inqu (3) 1563 (A)
	1790: umem n-ro s.am inqu (3) 1571 (R)
	2254: e-a- m.um r.e- paci (C) 1600 (A)
	1872: a-as o-e- f-a- Dest (3) 1617 (A)
	1908: isme o-e- o-c- poPr (3) 1625 (R)
	1764: m.a- n-el *.um cepe (3) 1633 (R)

Source [1664, 1879, 2265] -> Targets [1729 1881]
	1664: r.ma ereq r.de qubu (C) 1519 (A)
	2265: r.er cele iaem mere (S) 1526 (Q)
	1667: r.** *tmq r.de qubu (C) 1524 (A)
	1879: t*li umum ioum sesi (C) 1532 (R)
	1881: teli umum ioum sesi (C) 1534 (R)
	1695: t.li n-i- s.s- plap (C) 1548 (R)
	1701: r.is n-i- s.re plap (C) 1550 (R)
	2219: r.el n-i- etne HPFP (3) 1552 (R)
	1707: r.is n-i- s.em plap (C) 1554 (R)
	1710: r.*l n-i- s.em plap (C) 1557 (R)
	1715: r.** n-i- s.o- nura (C) 1561 (R)
	2106: r.er r.n- s.em plap (C) 1561 (R)
	1722: r.is n-i- s.m, plap (C) 1564 (R)
	1729: s.is r.n- s.a- plap (C) 1572 (R)

Source [1665, 1807, 1858] -> Targets [2115]
	1858: t.e- s.di c-t. luPo (C) 1518 (R)
	1665: n-e- a.ne s.iq erpe (C) 1520 (Q)
	1807: m-us o-66 e-en suta (3) 1581 (R)
	1808: m-us a-37 e-en suxt (3) 1585 (R)
	1809: gei- is37 e-en suta (3) 1591 (R)
	1746: o-ia e.i- e-en suta (3) 1593 (R)
	1810: o-ia e.i- e-en suta (3) 1594 (R)
	1811: n-e- is37 e-en suta (3) 1596 (A)
	1812: m,e- is37 e-en suta (3) 1601 (R)
	2278: m,e- in48 e-en suta (3) 1601 (A)
	1821: o-ia amm. e-en sune (3) 1602 (R)
	1813: o-ia amm. e-en sune (3) 1602 (R)
	1814: hius is37 e-en suta (3) 1603 (A)
	1815: t.e- e.id e-en suta (3) 1606 (R)
	1755: o-ia amm. e-en sune (3) 1607 (R)
	1816: t.e- e.id e-en maiu (3) 1607 (R)
	1817: t.e- e.id e-en suta (3) 1607 (R)
	2151: o-ia amm. e-en sune (3) 1607 (R)
	1818: o-ia amm. e-en sune (3) 1608 (R)
	1822: n-m- e.s- e-en maiu (3) 1618 (R)
	2115: n-m- amu- o-s. dite (3) 1629 (R)

Source [1692, 1694, 2198] -> Targets [1716 1800]
	1694: emim osn- ete- asre (C) 1547 (A)
	2198: uio- osa* ete- asre (C) 1547 (A)
	1692: uio- osan ete- asre (C) 1547 (A)
	1934: s-e- osan ete- asre (C) 1551 (A)
	1712: s-e- osn- iei- asre (C) 1559 (A)
	1716: s-e- osn- iei- asre (C) 1561 (A)
	1800: uio- ncnc uso- hisi (3) 1647 (R)

Source [1781, 2076, 2211] -> Targets [2254 1790 1764 1908]
	2211: niea irs- a.n- nute (C) 1507 (T)
	1883: ++++ m.ru s.am nute (C) 1537 (R)
	1884: o-et m.a- s.am nute (C) 1537 (R)
	1682: o-et m.um s.am nute (C) 1541 (R)
	1687: o-et m.de s.um nute (C) 1545 (R)
	2076: i-i- nti- lae) plau (7) 1550 (A)
	1781: t.s? a-em s.i- quui (3) 1552 (A)
	2104: t.s* i-is s.b- inqu (3) 1556 (A)
	2105: t.s? **eu s.am inqu (3) 1560 (A)
	2148: t.s? o*eu s.am inqu (3) 1560 (R)
	1784: o-,& ,&t. s.am inqu (3) 1561 (A)
	1862: i-i- o-e- s.i- inqu (3) 1563 (A)
	1790: umem n-ro s.am inqu (3) 1571 (R)
	2254: e-a- m.um r.e- paci (C) 1600 (A)
	1872: a-as o-e- f-a- Dest (3) 1617 (A)
	1908: isme o-e- o-c- poPr (3) 1625 (R)
	1764: m.a- n-el *.um cepe (3) 1633 (R)

Source [1825, 2153] -> Targets [1800]
	1825: s.o: a.ce uso- tesc (C) 1468 (A)
	2153: s.o: a.ce uso- tesc (C) 1498 (A)
	1801: o-r- ncnc uso- hisi (3) 1626 (R)
	2189: o-r- ncnc uso- hisi (3) 1626 (R)
	1799: o-r- ncnc uso- hisi (3) 1639 (R)
	1800: uio- ncnc uso- hisi (3) 1647 (R)

Source [1842, 1844] -> Targets [1846 2194 1962]
	1844: erre rie- umio legi (C) 1495 (Q)
	1847: r-l- rie- umio legi (C) 1520 (Q)
	2194: r-re i-re u*io legi (C) 1520 (Q)
	1846: r-re i-e- umio legi (C) 1520 (Q)
	1842: umha est: i-a- nome (3) 1535 (R)
	2255: r-l- 30b- i-a- sepe (7) 1587 (A)
	1963: amsi s,vt i-a- ctho (3) 1608 (R)
	1962: amsi s,vt i-a- ctho (3) 1619 (R)

Source [1856, 1857] -> Targets [2271]
	1856: s,e- s.ie i-i- tuve (3) 1566 (A)
	1857: s,e- s.ie i-i- tuve (3) 1566 (A)
	2180: t,a- i-um i-i- etla (C) 1576 (R)
	2271: t,a- i-um i-i- etla (C) 1577 (R)

Source [1880, 1885] -> Targets [1899 2254 1688 1897 1898]
	1880: isus ioer r.u- abfi (C) 1534 (R)
	1882: isge ioer r.u- dein (C) 1536 (R)
	1885: ++++ ++++ r.n- seFI (C) 1538 (R)
	1886: edus inam r.ra seFI (C) 1540 (R)
	1887: edus inam r.rò muFI (C) 1543 (A)
	1888: edus inam r.i- runu (C) 1543 (A)
	1684: edus inam r.ne suFi (C) 1543 (A)
	1690: n-re a.am r.n- stsp (C) 1545 (R)
	1688: edus inam m.t, seui (C) 1545 (A)
	1894: n-re a.es r.i- stsp (C) 1549 (R)
	1895: n-re a.a- r.e- stsp (C) 1550 (R)
	1900: n-re a.ua r.ro stra (C) 1553 (R)
	1891: n-e, a.ua r.i- umra (C) 1558 (R)
	1903: n-e, a.s, r.mi umra (C) 1561 (R)
	1904: n-e, a.a- r.mi umra (C) 1563 (R)
	1896: n-re a.a- r.i- umra (C) 1568 (R)
	1897: n-r* 𝜈.ί- umam 𝜎𝜒𝜏𝛼 (C) 1574 (R)
	1898: n-e, a.a- r.a, stra (C) 1578 (R)
	2254: e-a- m.um r.e- paci (C) 1600 (A)
	1753: iti* a.s, r.u- stsp (C) 1601 (A)
	1899: itit a.s, r.u- stsp (C) 1629 (R)

Source [2063, 2160, 2178, 2217] -> Targets [2115 2190 2067]
	2217: i-m- o-em erlo trtr (3) 1550 (Q)
	2160: u-a, ,&i- o-i- EsQu (3) 1564 (R)
	2063: e-,& a.20 ,&o: noEt (3) 1566 (R)
	2158: u-a, eti- o-ui EsQu (3) 1567 (R)
	2157: a-ad m,a- o-i- EsQu (3) 1570 (R)
	2073: l-in 2.a. ,&o: noEt (3) 1573 (R) 
	2161: a-ad m,a- o-i- EsQu (3) 1573 (R)
	2162: a-ad m,a- o-i- EsQu (3) 1578 (R)
	1919: l-in 2.a. ,&o: noEt (3) 1579 (A)
	2262: a-ad t.n- i-s. motu (3) 1580 (R)
	2258: eto- m,a- i-ae depr (3) 1581 (R)
	2178: eme- o-uè n-a- ctTe (3) 1582 (R)
	2159: a-ad m,i- o-i- EsQu (3) 1583 (R)
	2190: eto- m,a- i-ae depr (3) 1583 (R)
	2069: l-in 42a. ,&o: noEt (3) 1589 (Q)
	2176: i-m- o-a- n-a- ctTe (3) 1588 (R)
	2177: aro- umam n-a- diva (3) 1590 (R)
	2163: a&o- usus o-i- EsQu (3) 1591 (R)
	2181: aro- umm, o-i- diie (3) 1595 (R)
	2067: l-in 42a. ,&o: noEt (3) 1595 (R)
	2175: i-m- t.PI o-i- cite (3) 1598 (R)
	2182: ròi- t.P- o-i- cite (3) 1603 (R)
	2164: a-ad m,i- o-i- EsQu (3) 1605 (A)
	2179: e-,& t.PI o-i- dite (3) 1611 (R)
	2183: e-r, i-i- o-s. dite (3) 1616 (R)
	2115: n-m- amu- o-s. dite (3) 1629 (R)

Source 1616 -> Targets [2200]
	1616: m.la e*ib ibin titi (C) 1489 (Q)
	2200: uma- etie b*in titi (C) 1490 (Q)

Source 2201 -> Targets [2200]
	2200: uma- etie b*in titi (C) 1490 (Q)
	2201: ummo *&ie b*in tica (C) 1489 (Q)

Source 1624 -> Targets [1828]
	1624: a8a7 neio 3040 3020 (C) 1494 (A)
	1830: a8a7 neio 3040 3020 (C) 1500 (A)
	1833: a8a7 neio 3040 3020 (C) 1507 (A)
	1829: a8a7 neio 3040 3020 (C) 1511 (A)
	1828: a8a7 m.a. 3040 3020 (C) 1516 (A)

Source 1626 -> Targets [1638]
	1626: i-it e*te r.ur teri (C) 1494 (Q)
	1632: unom ner- r.ur reho (C) 1498 (A)
	1638: i-ur tete r.ur teri (C) 1510 (Q)

Source 1826 -> Targets [1656]
	1826: r.ae t.tu s.us foce (C) 1499 (R)
	1656: r.ae temq r.te quet (C) 1516 (R)

Source 1640 -> Targets [2103]
	1640: amo- rame esr. teco (C) 1503 (R)
	2103: buua popu orti *eco (3) 1549 (A)

Source 2118 -> Targets [2233]
	2118: heur a.di e-ne moui (C) 1503 (Q)
	1795: heur a.di e-ne moui (C) 1505 (Q)
	2233: dimi iup- e-ne infe (3) 1574 (R)

Source 1805 -> Targets [1824]
	1805: s.i. maas nar- p*te (C) 1508 (Q)
	1824: s.t. maas ore- nisu (3) 1515 (A)

Source 1655 -> Targets [1877]
	1655: erde n-r/ n.t- br*I (C) 1516 (R)
	1877: erde enr. n.y- br*I (C) 1519 (R)

Source 1918 -> Targets [1948]
	1918: miea s.us a.r- *Dre (C) 1518 (R)
	1948: o.ui s.us &c*: masu (3) 1522 (R)

Source 1870 -> Targets [1843]
	1870: a1a. enon 6142 polu (3) 1521 (A)
	1869: a1a. sate 6142 stta (3) 1527 (A)
	1832: .1a. sate 6142 stta (3) 1531 (A)
	1831: .1a. sate 6142 stta (3) 1534 (A)
	1843: .1a. ine, 6142 sota (3) 1538 (A)

Source 1772 -> Targets [1764]
	1772: o.u: p-ce a-o- tesu (C) 1526 (A)
	1761: m.a- n-el a-o- misi (3) 1619 (A)
	1764: m.a- n-el *.um cepe (3) 1633 (R)

Source 1670 -> Targets [1679]
	1670: r.on ernt e.ue si&q (C) 1527 (R)
	1679: r,ae era, e.ue si&q (C) 1534 (A)

Source 1864 -> Targets [1865]
	1864: ß.ß. imun umor gein (C) 1533 (A)
	1865: ß.ß. imn- umor gein (C) 1539 (R)

Source 1802 -> Targets [2270]
	1802: n-i- .*s. l.on puyc (3) 1545 (A)
	2269: e-au .7s. leux &cpo (3) 1551 (R)
	2270: e-au .7s. leux &cpo (3) 1552 (R)

Source 2240 -> Targets [1729]
	2240: s.rò I.s. s.a- rato (C) 1548 (R)
	1729: s.is r.n- s.a- plap (C) 1572 (R)

Source 2070 -> Targets [1718]
	2070: o-fa i.o. o.m- Fiad (3) 1548 (R)
	1718: elt- i.o. o.o, DEFI (3) 1561 (R)

Source 1779 -> Targets [1594]
	1779: 1.gi ume& inte **es (3) 1549 (A)
	1594: dii- umy- inte prdu (3) 1550 (A)

Source 2257 -> Targets [2253]
	2257: e-i- t.i, uro- di&p (3) 1551 (R)
	2252: e-i- t.i, uro- di&p (3) 1552 (R)
	2253: o-sa m.ad uro- di&p (3) 1555 (R)

Source 1952 -> Targets [1902]
	1952: Lamu umm. m.n- cuar (3) 1551 (A)
	1902: Lamu umm. m.n- cuar (3) 1552 (A)

Source 1780 -> Targets [2235]
	1780: usa- a-u- a.a? Ingi (3) 1551 (A)
	1782: usa- a-u- a.a? Ingi (3) 1552 (A)
	2235: usa- adi- s:li so24 (3) 1568 (R)

Source 2077 -> Targets [2280]
	2077: oss: a.x. ioey guAn (3) 1551 (A)
	2280: oss: x.aq ioey geAn (3) 1556 (A)

Source 2064 -> Targets [2068]
	2064: o-a- erta s-a, fano (3) 1552 (R)
	2068: elle erta uee- uign (3) 1553 (R)

Source 2165 -> Targets [1778 2173]
	2165: r-es e.ro s.n. rusu (C) 1553 (R)
	2172: r-es e.ro s.n. rusu (C) 1554 (R)
	2168: s,in um,& s.n. rusu (C) 1558 (R)
	2166: s,si um,& s.n. rusu (C) 1563 (A)
	2167: s,in um,& s.0. Vida (3) 1569 (R)
	2169: s,in um,& s.n. rusu (3) 1576 (R)
	1777: a-e, um,& i.t. trSe (3) 1585 (R)
	2171: s,is a-de s.0. Vida (3) 1587 (R)
	1778: ear- um,& itrò Cusc (3) 1598 (A)
	2173: e-E- iot, s.0. Vive (3) 1601 (A)

Source 1708 -> Targets [1728]
	1708: a-u- ero- m.c- mafo (3) 1556 (A)
	1711: a-u- ++++ m.c- mafo (3) 1557 (A)
	1964: ita. ero- m.c- igfo (3) 1558 (A)
	1955: ita. t,rè m.c- niet (3) 1561 (A)
	1969: ita. t:rè m.c- niti (3) 1562 (A)
	1973: ita. t:rè m.c- niti (3) 1564 (A)
	1728: ita. tuta dea- ilnu (3) 1569 (A)

Source 1783 -> Targets [1789]
	1783: i,ut aeam a-nt tiPo (3) 1560 (Q)
	1786: i,ut aeam a-nt tiPo (3) 1562 (A)
	1788: i,ut aeam a-nt tiPo (3) 1565 (Q)
	1725: i,ut aeam a-nt tiPo (3) 1566 (A)
	1789: i,ut aeam a-nt tiPo (3) 1569 (A)

Source 2087 -> Targets [1933]
	2087: r-i- turu a.e- alpl (3) 1562 (R)
	1933: n.c. 2.l. a.e- sibl (3) 1650 (A)

Source 1912 -> Targets [1968]
	1912: o-n- onre oso, lope (3) 1564 (A)
	1968: o-n- onre oso, lope (3) 1567 (A)

Source 1953 -> Targets [1959]
	1953: ine- o,o- aeu- uiri (3) 1566 (R)
	1960: é-a- o,o- eti- salu (3) 1581 (R)
	1961: i-t, o,o- isin made (3) 1591 (R)
	1959: tit, o,o- L.o. nèma (C) 1594 (R)

Source 1727 -> Targets [2197]
	1727: o.d. noia TOos brno (3) 1567 (R)
	2197: o.ad noia TOos brno (3) 1568 (R)

Source 1974 -> Targets [1741]
	1974: a:i, imte t.ui stma (3) 1569 (A)
	1911: a:i, imte t.ui stma (3) 1572 (A)
	1970: a:i, imte t.t, stma (3) 1577 (A)
	1741: a-on im*e sao- 15ra (3) 1584 (A)

Source 1730 -> Targets [1958 2281]
	1730: n-ni umut o,io cequ (7) 1573 (R)
	2281: niri umut ine, artr (3) 1582 (R)
	1958: niri umut ine, artr (3) 1582 (R)

Source 1836 -> Targets [2215]
	1836: i,uo e.la enhe lach (3) 1574 (R)
	2215: len- alie enhe lach (3) 1575 (A)

Source 1965 -> Targets [1972]
	1965: e-a. raa- umo- lial (3) 1574 (R)
	1971: e-a. raet umo- lial (3) 1586 (R)
	1972: e-in m;ua umo- alnu (3) 1620 (R)

Source 2208 -> Targets [2237]
	2208: u-u- r.e- u,ut nadi (3) 1575 (R)
	2268: u-u- r.e- u,ut nadi (3) 1581 (R)
	2237: u-u- r.e- u*ut nadi (3) 1585 (R)

Source 1733 -> Targets [1739]
	1733: seun aol, mee, FIpr (3) 1576 (A)
	1739: o-ui aol, mee, FIge (3) 1584 (A)

Source 2203 -> Targets [1852]
	2203: a-c- i-mo s,e- spqu (3) 1577 (R)
	1852: a-c- i-mo s,e- spqu (3) 1578 (R)

Source 1734 -> Targets [1752]
	1734: r.i- e,n- s,ri Quas (3) 1577 (A)
	1737: r.i- e,n- s,ri Quas (3) 1580 (R)
	1743: r.i- e,n- s,ri Quas (3) 1587 (R)
	1748: r.i- e,n- s,ri Quas (3) 1594 (R)
	1752: r.a- ++++ s,i- Quas (3) 1601 (R)

Source 2225 -> Targets [2226]
	2225: r,e, asi- i,ke uepe (3) 1579 (A)
	2223: r,e, asi- i,ke uepe (3) 1584 (A)
	2226: e,rs e,we i,ke thta (3) 1589 (A)

Source 2242 -> Targets [2241]
	2242: i-a- s.uo u-a- buEx (3) 1580 (R)
	2241: i-a- s.uo u-a- *jre (3) 1581 (R)

Source 2245 -> Targets [2243]
	2245: p.do r.a- e*q: *tSu (C) 1587 (R)
	2243: emst r.es emq; stSu (C) 1593 (R)

Source 1950 -> Targets [1752]
	1950: r.a- n,mi e-lo delo (3) 1599 (R)
	1752: r.a- ++++ s,i- Quas (3) 1601 (R)

Source 2229 -> Targets [2228]
	2229: ofnd 6d2c g.re thTh (3) 1609 (A)
	2222: sene *d** g.ly thTh (3) 1615 (A)
	2228: d,i- est- g.l- thTh (3) 1630 (A)

Source 2261 -> Targets [1980 1979]
	1979: r.t. m-en r-a- vode (3) 1622 (A)
	2261: r.t. m-en r-a- vode (3) 1614 (A)
	1980: asV. s.z- r-a- Goin (3) 1622 (A)

Source 1758 -> Targets [1760]
	1758: inn- geus eure FIsi (3) 1616 (R)
	1760: inn- geus eure FIsi (3) 1619 (R)
